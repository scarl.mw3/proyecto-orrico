<?php include("Conexion/db.php"); ?>

<?php include('vistas/parte_sup.php'); ?>

<main class="container p-4">
<div class="container">
<h1>Catálogo</h1>

            <a href="1add_catalogo.php" class="btn btn-success ">
                <i class="fas fa-plus-square "></i> Nuevo
              </a>
              <hr>

              <div class="form-group">
              <form action="Buscar_c.php" method="POST">
             <input type="text" name="prod" class="form" placeholder="Producto" autofocus>
             <input type="submit" class="btn btn-primary" value="Buscar" >
              </form>
              </div>          
      
      <hr>


      <!--/////////////////////////////////////////////////////////////-->


    <div class="col-md" style="overflow:auto">
      <table class="table table-bordered">
        <thead>
          <tr>
            <th>Id</th>
            <th>Producto</th>
            <th>Descripcion</th>
            <th>Precio</th>
            <th>Fecha de creacion</th>
          </tr>
        </thead>
        <tbody>

          <?php
          $query = "SELECT * FROM catalogo";
          $result_tasks = mysqli_query($conn, $query);    

          while($row = mysqli_fetch_assoc($result_tasks)) { ?>
          <tr>
            <td><?php echo $row['id_c']; ?></td>
            <td><?php echo $row['producto']; ?></td>
            <td><?php echo $row['descripcion']; ?></td>
            <td><?php echo $row['precio']; ?></td>
            <td><?php echo $row['Fecha']; ?></td>
            <td>
              <a href="1edit_catalogo.php?id=<?php echo $row['id_c']?>" class="btn btn-secondary">
                <i class="fas fa-marker"></i>
              </a>
              <a href="1delete_catalogo.php?id=<?php echo $row['id_c']?>" class="btn btn-danger">
                <i class="far fa-trash-alt"></i>
              </a>
            </td>
          </tr>
          <?php } ?>
        </tbody>
      </table>
    </div>
  </div>
</main>

<?php include('vistas/parte_inf.php'); ?>
