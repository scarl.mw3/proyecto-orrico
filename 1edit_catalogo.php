<?php
include('Conexion/db.php');
$producto= '';
$descripcion= '';
$precio = '';


if  (isset($_GET['id'])) {
  $id = $_GET['id'];
  $query = "SELECT * FROM catalogo WHERE id_c=$id";
  $result = mysqli_query($conn, $query);
  if (mysqli_num_rows($result) == 1) {
    $row = mysqli_fetch_array($result);
    $producto = $row['producto'];
    $descripcion = $row['descripcion'];
    $precio = $row['precio'];
  }
}

if (isset($_POST['update'])) {
  $id = $_GET['id'];
  $producto= $_POST['producto'];
  $descripcion = $_POST['descripcion'];
  $precio= $_POST['precio'];

  $query = "UPDATE catalogo set producto = '$producto', descripcion = '$descripcion', precio = '$precio' WHERE id_c=$id";
  mysqli_query($conn, $query);
  $_SESSION['message'] = 'Task Updated Successfully';
  $_SESSION['message_type'] = 'warning';
  header('Location: 1Principal_catalogo.php');
}

?>
<?php include('vistas/parte_sup.php'); ?>
<div class="container p-4">
  <div class="row">
    <div class="col-md-4 mx-auto">
      <div class="card card-body">
      <form action="1edit_catalogo.php?id=<?php echo $_GET['id']; ?>" method="POST">
        <div class="form-group">
          <input name="producto" type="text" class="form-control" value="<?php echo $producto; ?>" placeholder="Producto">
        </div>
        <div class="form-group">
          <input name="descripcion" type="text" class="form-control" value="<?php echo $descripcion; ?>" placeholder="Descripcion">
        </div>
        <div class="form-group">
          <input name="precio" type="text" class="form-control" value="<?php echo $precio; ?>" placeholder="Precio">
        </div>
        <button class="btn-success" name="update">
        <i class="fas fa-marker"></i> Actualizar
</button>
      </form>
      </div>
    </div>
  </div>
</div>
<?php include('vistas/parte_inf.php'); ?>
