<?php include("Conexion/db.php"); ?>

<?php include('vistas/parte_sup.php'); ?>

<main class="container p-4">
<div class="">
<h1>Inventario de Materiales</h1>

<a href="2add_inventario_m.php" class="btn btn-success ">
                <i class="fas fa-plus-square "></i> Nuevo
              </a>
              <hr>

              <div class="form-group">
              <form action="Buscar_m.php" method="POST">
             <input type="text" name="prod" class="form" placeholder="Nombre" autofocus>
             <input type="submit" class="btn btn-primary" value="Buscar" >
              </form>
              </div>  
      
      <hr>


    <div class="col-md" style="overflow:auto">
      <table class="table table-bordered">
        <thead>
          <tr>
            <th>Id</th>
            <th>Nombre</th>
            <th>Cantidad</th>
            <th>Descripcion</th>
            <th>Precio</th>
            <th>Fecha de creacion</th>
          </tr>
        </thead>
        <tbody>

          <?php
          $query = "SELECT * FROM inventario_m";
          $result_tasks = mysqli_query($conn, $query);    

          while($row = mysqli_fetch_assoc($result_tasks)) { ?>
          <tr>
            <td><?php echo $row['id_m']; ?></td>
            <td><?php echo $row['nombre']; ?></td>
            <td><?php echo $row['cantidad']; ?></td>
            <td><?php echo $row['descripcion']; ?></td>
            <td><?php echo $row['precio']; ?></td>
            <td><?php echo $row['Fecha']; ?></td>
            <td>
              <a href="2edit_inventario_m.php?id=<?php echo $row['id_m']?>" class="btn btn-secondary">
                <i class="fas fa-marker"></i>
              </a>
              <a href="2delete_inventario_m.php?id=<?php echo $row['id_m']?>" class="btn btn-danger">
                <i class="far fa-trash-alt"></i>
              </a>
            </td>
          </tr>
          <?php } ?>
        </tbody>
      </table>
    </div>
  </div>
</main>

<?php include('vistas/parte_inf.php'); ?>
