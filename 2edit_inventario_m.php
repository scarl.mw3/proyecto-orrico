<?php
include('Conexion/db.php');
$nombre= '';
$cantidad= '';
$descripcion = '';
$precio = '';


if  (isset($_GET['id'])) {
  $id = $_GET['id'];
  $query = "SELECT * FROM inventario_m WHERE id_m=$id";
  $result = mysqli_query($conn, $query);
  if (mysqli_num_rows($result) == 1) {
    $row = mysqli_fetch_array($result);
    $nombre = $row['nombre'];
    $cantidad = $row['cantidad'];
    $descripcion = $row['descripcion'];
    $precio = $row['precio'];
  }
}

if (isset($_POST['update'])) {
  $id = $_GET['id'];
  $nombre= $_POST['nombre'];
  $cantidad = $_POST['cantidad'];
  $descripcion = $_POST['descripcion'];
  $precio= $_POST['precio'];

  $query = "UPDATE inventario_m set nombre = '$nombre', cantidad = '$cantidad', descripcion = '$descripcion', precio = '$precio' WHERE id_m=$id";
  mysqli_query($conn, $query);
  $_SESSION['message'] = 'Task Updated Successfully';
  $_SESSION['message_type'] = 'warning';
  header('Location: 2Principal_inventario_m.php');
}

?>
<?php include('vistas/parte_sup.php'); ?>
<div class="container p-4">
  <div class="row">
    <div class="col-md-4 mx-auto">
      <div class="card card-body">
      <form action="2edit_inventario_m.php?id=<?php echo $_GET['id']; ?>" method="POST">
        <div class="form-group">
          <input name="nombre" type="text" class="form-control" value="<?php echo $nombre; ?>" placeholder="Nombre">
        </div>
        <div class="form-group">
          <input name="cantidad" type="text" class="form-control" value="<?php echo $cantidad; ?>" placeholder="Cantidad">
        </div>
        <div class="form-group">
          <input name="descripcion" type="text" class="form-control" value="<?php echo $descripcion; ?>" placeholder="Descripcion">
        </div>
        <div class="form-group">
          <input name="precio" type="text" class="form-control" value="<?php echo $precio; ?>" placeholder="Precio">
        </div>
        <button class="btn-success" name="update">
        <i class="fas fa-marker"></i> Actualizar
</button>
      </form>
      </div>
    </div>
  </div>
</div>
<?php include('vistas/parte_inf.php'); ?>
