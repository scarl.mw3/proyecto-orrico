<?php include("Conexion/db.php"); ?>

<?php include('vistas/parte_sup.php'); ?>

<main class="container p-4">
<div class="">
<h1>Inventario de Herramientas</h1>

<a href="3add_inventario_h.php" class="btn btn-success ">
                <i class="fas fa-plus-square "></i> Nuevo
              </a>
              <hr>

              <div class="form-group">
              <form action="Buscar_h.php" method="POST">
             <input type="text" name="prod" class="form" placeholder="Nombre" autofocus>
             <input type="submit" class="btn btn-primary" value="Buscar" >
              </form>
              </div>  
      
      <hr>


    <div class="col-md" style="overflow:auto">
      <table class="table table-bordered">
        <thead>
          <tr>
            <th>Id</th>
            <th>Nombre</th>
            <th>Descripcion</th>
            <th>Marca</th>
          </tr>
        </thead>
        <tbody>

          <?php
          $query = "SELECT * FROM inventario_h";
          $result_tasks = mysqli_query($conn, $query);    

          while($row = mysqli_fetch_assoc($result_tasks)) { ?>
          <tr>
            <td><?php echo $row['id_h']; ?></td>
            <td><?php echo $row['nombre']; ?></td>
            <td><?php echo $row['descripcion']; ?></td>
            <td><?php echo $row['marca']; ?></td>
            <td>
              <a href="3edit_inventario_h.php?id=<?php echo $row['id_h']?>" class="btn btn-secondary">
                <i class="fas fa-marker"></i>
              </a>
              <a href="3delete_inventario_h.php?id=<?php echo $row['id_h']?>" class="btn btn-danger">
                <i class="far fa-trash-alt"></i>
              </a>
            </td>
          </tr>
          <?php } ?>
        </tbody>
      </table>
    </div>
  </div>
</main>

<?php include('vistas/parte_inf.php'); ?>
