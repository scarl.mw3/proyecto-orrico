<?php
include('Conexion/db.php');
$nombre= '';
$descripcion = '';
$marca = '';


if  (isset($_GET['id'])) {
  $id = $_GET['id'];
  $query = "SELECT * FROM inventario_h WHERE id_h=$id";
  $result = mysqli_query($conn, $query);
  if (mysqli_num_rows($result) == 1) {
    $row = mysqli_fetch_array($result);
    $nombre = $row['nombre'];
    $descripcion = $row['descripcion'];
    $marca = $row['marca'];
  }
}

if (isset($_POST['update'])) {
  $id = $_GET['id'];
  $nombre= $_POST['nombre'];
  $descripcion = $_POST['descripcion'];
  $marca= $_POST['marca'];

  $query = "UPDATE inventario_h set nombre = '$nombre', descripcion = '$descripcion', marca = '$marca' WHERE id_h=$id";
  mysqli_query($conn, $query);
  $_SESSION['message'] = 'Task Updated Successfully';
  $_SESSION['message_type'] = 'warning';
  header('Location: 3Principal_inventario_h.php');
}

?>
<?php include('vistas/parte_sup.php'); ?>
<div class="container p-4">
  <div class="row">
    <div class="col-md-4 mx-auto">
      <div class="card card-body">
      <form action="3edit_inventario_h.php?id=<?php echo $_GET['id']; ?>" method="POST">
        <div class="form-group">
          <input name="nombre" type="text" class="form-control" value="<?php echo $nombre; ?>" placeholder="Nombre">
        </div>
        <div class="form-group">
          <input name="descripcion" type="text" class="form-control" value="<?php echo $descripcion; ?>" placeholder="Descripcion">
        </div>
        <div class="form-group">
          <input name="marca" type="text" class="form-control" value="<?php echo $marca; ?>" placeholder="Marca">
        </div>
        <button class="btn-success" name="update">
        <i class="fas fa-marker"></i> Actualizar
</button>
      </form>
      </div>
    </div>
  </div>
</div>
<?php include('vistas/parte_inf.php'); ?>
