<?php include("Conexion/db.php"); ?>

<?php include('vistas/parte_sup.php'); ?>

<main class="container p-4">
<div class="">
<h1>Empleados</h1>

<a href="4add_empleados.php" class="btn btn-success ">
                <i class="fas fa-plus-square "></i> Nuevo
              </a>
              <hr>

              <div class="form-group">
              <form action="Buscar_e.php" method="POST">
             <input type="text" name="prod" class="form" placeholder="Nombre" autofocus>
             <input type="submit" class="btn btn-primary" value="Buscar" >
              </form>
              </div>  
      
      <hr>


    <div class="col-md" style="overflow:auto">
      <table class="table table-bordered">
        <thead>
          <tr>
            <th>RFC</th>
            <th>Nombre</th>
            <th>Apellido paterno</th>
            <th>Apellido materno</th>
            <th>Correo</th>
            <th>Telefono</th>
            <th>Calle</th>
            <th>Colonia/fracc.</th>
            <th>Numero de casa</th>
          </tr>
        </thead>
        <tbody>

          <?php
          $query = "SELECT * FROM empleados";
          $result_tasks = mysqli_query($conn, $query);    

          while($row = mysqli_fetch_assoc($result_tasks)) { ?>
          <tr>
            <td><?php echo $row['rfc']; ?></td>
            <td><?php echo $row['nombre']; ?></td>
            <td><?php echo $row['ape1']; ?></td>
            <td><?php echo $row['ape2']; ?></td>
            <td><?php echo $row['correo']; ?></td>
            <td><?php echo $row['telefono']; ?></td>
            <td><?php echo $row['calle']; ?></td>
            <td><?php echo $row['fracc_col']; ?></td>
            <td><?php echo $row['numero_casa']; ?></td>
            <td>
              <a href="4edit_empleados.php?id=<?php echo $row['rfc']?>" class="btn btn-secondary">
                <i class="fas fa-marker"></i>
              </a>
              <a href="4delete_empleados.php?id=<?php echo $row['rfc']?>" class="btn btn-danger">
                <i class="far fa-trash-alt"></i>
              </a>
            </td>
          </tr>
          <?php } ?>
        </tbody>
      </table>
    </div>
  </div>
</main>

<?php include('vistas/parte_inf.php'); ?>
