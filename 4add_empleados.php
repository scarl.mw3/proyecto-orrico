<?php include('vistas/parte_sup.php'); ?>


    <div class="col-md">
      <!-- MESSAGES -->

      <?php if (isset($_SESSION['message'])) { ?>
      <div class="alert alert-<?= $_SESSION['message_type']?> alert-dismissible fade show" role="alert">
        <?= $_SESSION['message']?>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <?php session_unset(); } ?>

      <!-- ADD TASK FORM -->
      
     

      <div class="card card-body">
        <form action="4save_empleados.php" method="POST">
        <div class="form-group">
            <input type="text" name="rfc" class="form-control" placeholder="RFC" autofocus>
          </div>
          <div class="form-group">
            <input type="text" name="nombre" class="form-control" placeholder="Nombre" autofocus>
          </div>
          <div class="form-group">
            <input type="text" name="ape1" class="form-control" placeholder="Apellido paterno" autofocus>
          </div>
          <div class="form-group">
            <input type="text" name="ape2" class="form-control" placeholder="Apellido materno" autofocus>
          </div>
          <div class="form-group">
            <input type="text" name="correo" class="form-control" placeholder="Correo" autofocus>
          </div>
          <div class="form-group">
            <input type="text" name="telefono" class="form-control" placeholder="Telefono" autofocus>
          </div>
          <div class="form-group">
            <input type="text" name="calle" class="form-control" placeholder="Calle" autofocus>
          </div>
          <div class="form-group">
            <input type="text" name="fracc_col" class="form-control" placeholder="Colonia o Fraccionamiento" autofocus>
          </div>
          <div class="form-group">
            <input type="text" name="numero_casa" class="form-control" placeholder="Numero" autofocus>
          </div>
          <input type="submit" name="save_task" class="btn btn-success btn-block" value="Save Task">
        </form>
      </div>
    </div>

    <?php include('vistas/parte_inf.php'); ?>