<?php
include('Conexion/db.php');
$rfc= '';
$nombre = '';
$ape1 = '';
$ape2= '';
$correo = '';
$telefono = '';
$calle= '';
$fracc_col = '';
$numero_casa = '';


if  (isset($_GET['id'])) {
  $id = $_GET['id'];
  $query = "SELECT * FROM empleados WHERE rfc='$id'";
  $result = mysqli_query($conn, $query);
  if (mysqli_num_rows($result) == 1) {
    $row = mysqli_fetch_array($result);
    $rfc = $row['rfc'];
    $nombre = $row['nombre'];
    $ape1 = $row['ape1'];
    $ape2 = $row['ape2'];
    $correo = $row['correo'];
    $telefono = $row['telefono'];
    $calle = $row['calle'];
    $fracc_col = $row['fracc_col'];
    $numero_casa = $row['numero_casa'];
  }
}

if (isset($_POST['update'])) {
  $id1 = $_GET['id'];
  $rfc1= $_POST['rfc'];
  $nombre1 = $_POST['nombre'];
  $ape11= $_POST['ape1'];
  $ape21= $_POST['ape2'];
  $correo1 = $_POST['correo'];
  $telefono1= $_POST['telefono'];
  $calle1= $_POST['calle'];
  $fracc_col1 = $_POST['fracc_col'];
  $numero_casa1= $_POST['numero_casa'];

  $query1 = "UPDATE empelados set rfc = '$rfc1', nombre = '$nombre1', ape1 = '$ape11',ape2 = '$ape21', correo = '$correo1', telefono = '$telefono1',calle = '$calle1', fracc_col = '$fracc_col1', numero_casa = '$numero_casa1' WHERE rfc='$id1'";
  mysqli_query($conn, $query1);
  $_SESSION['message'] = 'Task Updated Successfully';
  $_SESSION['message_type'] = 'warning';
  header('Location: 4Principal_empleados.php');
}

?>
<?php include('vistas/parte_sup.php'); ?>
<div class="container p-4">
  <div class="row">
    <div class="col-md-4 mx-auto">
      <div class="card card-body">
      <form action="4edit_empleados.php?id=<?php echo $_GET['id']; ?>" method="POST">
        <div class="form-group">
          <input name="rfc" type="text" class="form-control " value="<?php echo $rfc; ?>" placeholder="RFC" required>
        </div>
        <div class="form-group">
          <input name="nombre" type="text" class="form-control" value="<?php echo $nombre; ?>" placeholder="Nombre">
        </div>
        <div class="form-group">
          <input name="ape1" type="text" class="form-control" value="<?php echo $ape1; ?>" placeholder="Apellido paterno">
        </div>
        <div class="form-group">
          <input name="ape2" type="text" class="form-control" value="<?php echo $ape2; ?>" placeholder="Apellido materno">
        </div>
        <div class="form-group">
          <input name="correo" type="text" class="form-control" value="<?php echo $correo; ?>" placeholder="Correo">
        </div>
        <div class="form-group">
          <input name="telefono" type="text" class="form-control" value="<?php echo $telefono; ?>" placeholder="Telefono">
        </div>
        <div class="form-group">
          <input name="calle" type="text" class="form-control" value="<?php echo $calle; ?>" placeholder="Calle">
        </div>
        <div class="form-group">
          <input name="fracc_col" type="text" class="form-control" value="<?php echo $fracc_col; ?>" placeholder="Colonia o Fraccionamiento">
        </div>
        <div class="form-group">
          <input name="numero_casa" type="text" class="form-control" value="<?php echo $numero_casa; ?>" placeholder="Numero">
        </div>
        <button class="btn-success" name="update">
        <i class="fas fa-marker"></i> Actualizar
</button>
      </form>
      </div>
    </div>
  </div>
</div>
<?php include('vistas/parte_inf.php'); ?>
