<?php include("Conexion/db.php"); ?>

<?php include('vistas/parte_sup.php'); ?>

<main class="container p-4">
<div class="container">
<h1>Resultado de la busqueda</h1>



<div class="col-md" style="overflow:auto">
      <table class="table table-bordered">
        <thead>
          <tr>
            <th>Id</th>
            <th>Nombre</th>
            <th>Cantidad</th>
            <th>Descripcion</th>
            <th>Precio</th>
            <th>Fecha de creacion</th>
          </tr>
        </thead>
        <tbody>

          <?php
          $prod = $_POST['prod'];
          $query = "SELECT * FROM inventario_m WHERE nombre LIKE '$prod%'";
          $result_tasks = mysqli_query($conn, $query);    

          while($row = mysqli_fetch_assoc($result_tasks)) { ?>
          <tr>
            <td><?php echo $row['id_m']; ?></td>
            <td><?php echo $row['nombre']; ?></td>
            <td><?php echo $row['cantidad']; ?></td>
            <td><?php echo $row['descripcion']; ?></td>
            <td><?php echo $row['precio']; ?></td>
            <td><?php echo $row['Fecha']; ?></td>
            <td>
              <a href="2edit_inventario_m.php?id=<?php echo $row['id_m']?>" class="btn btn-secondary">
                <i class="fas fa-marker"></i>
              </a>
              <a href="2delete_inventario_m.php?id=<?php echo $row['id_m']?>" class="btn btn-danger">
                <i class="far fa-trash-alt"></i>
              </a>
            </td>
          </tr>
          <?php } ?>
        </tbody>
      </table>
    </div>
  </div>
</main>

<?php include('vistas/parte_inf.php'); ?>
