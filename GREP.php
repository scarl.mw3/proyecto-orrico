
<?php
ob_clean();  
require('fpdf182/fpdf.php');
  


class PDF extends FPDF
{
// Cabecera de página
function Header()
{
    // Logo
    //$this->Image('Img/logo_pb.png',10,8,33);
    // Arial bold 15
    $this->SetFont('Helvetica','B',30);
    // Movernos a la derecha
    $this->Cell(80);
    // Título
    $this->Cell(30,10,'MADETTA',0,0,'C');
    // Salto de línea
    $this->Ln(20);

    $this->SetFont('Helvetica','B',8);

    
}

// Pie de página
function Footer()
{
    // Posición: a 1,5 cm del final
    $this->SetY(-15);
    // Arial italic 8
    $this->SetFont('Arial','I',8);
    // Número de página
    $this->Cell(0,10,'Madetta',0,0,'C');
    $this->Cell(0,10,'Pagina '.$this->PageNo().'/{nb}',0,0,'C');
}
}

$f1 = $_POST['f_inicio'];
$f2 = $_POST['f_final'];
require 'cn.php';

//////////////////////////////////////////////////////////////////

$consulta = "SELECT * FROM catalogo WHERE Fecha BETWEEN '$f1' AND '$f2'";
$result = $mysqli->query($consulta);

$consulta1 = "SELECT * FROM inventario_m WHERE Fecha BETWEEN '$f1' AND '$f2'";
$result1 = $mysqli->query($consulta1);

$consulta2 = "SELECT * FROM inventario_h WHERE Fecha BETWEEN '$f1' AND '$f2'";
$result2 = $mysqli->query($consulta2);

$consulta3 = "SELECT * FROM empleados WHERE Fecha BETWEEN '$f1' AND '$f2'";
$result3 = $mysqli->query($consulta3);
$consulta4 = "SELECT * FROM empleados WHERE Fecha BETWEEN '$f1' AND '$f2'";
$result4 = $mysqli->query($consulta4);
$consulta5 = "SELECT * FROM empleados WHERE Fecha BETWEEN '$f1' AND '$f2'";
$result5 = $mysqli->query($consulta5);
//////////////////////////////////////////////////////////////////


$pdf = new PDF();
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetFont('Arial','',8);
//////////////////////////////////////////////////////////////////

$pdf->Cell(0,10,'Tabla "Catalogo"',0,1,'C');

    $pdf->Cell(10, 10, 'id_c', 1, 0, 'C', 0);
    $pdf->Cell(65, 10, 'producto', 1, 0, 'C', 0);
    $pdf->Cell(75, 10, 'descripcion', 1, 0, 'C', 0);
    $pdf->Cell(20, 10, 'precio', 1, 0, 'C', 0);
    $pdf->Cell(20, 10, 'Fecha', 1, 1, 'C', 0);
while($row = $result->fetch_assoc()){
    $pdf->Cell(10, 10, $row['id_c'], 1, 0, 'C', 0);
    $pdf->Cell(65, 10, $row['producto'], 1, 0, 'C', 0);
    $pdf->Cell(75, 10, $row['descripcion'], 1, 0, 'C', 0);
    $pdf->Cell(20, 10, $row['precio'], 1, 0, 'C', 0);
    $pdf->Cell(20, 10, $row['Fecha'], 1, 1, 'C', 0);
}
//////////////////////////////////////////////////////////////////

$pdf->Cell(0,10,'Tabla "Inventario de Materiales"',0,1,'C');

    $pdf->Cell(10, 10, 'id_m', 1, 0, 'C' , 0);
    $pdf->Cell(50, 10, 'Nombre', 1, 0, 'C', 0);
    $pdf->Cell(20, 10, 'Cantidad', 1, 0, 'C', 0);
    $pdf->Cell(70, 10, 'Descripcion', 1, 0, 'C', 0);
    $pdf->Cell(20, 10, 'Precio', 1, 0, 'C', 0);
    $pdf->Cell(20, 10, 'Fecha', 1, 1, 'C', 0);
    while($row = $result1->fetch_assoc()){
    $pdf->Cell(10, 10, $row['id_m'], 1, 0, 'C', 0);
    $pdf->Cell(50, 10, $row['nombre'], 1, 0, 'C', 0);
    $pdf->Cell(20, 10, $row['cantidad'], 1, 0, 'C', 0);
    $pdf->Cell(70, 10, $row['descripcion'], 1, 0, 'C', 0);
    $pdf->Cell(20, 10, $row['precio'], 1, 0, 'C', 0);
    $pdf->Cell(20, 10, $row['Fecha'], 1, 1, 'C', 0);
}
//////////////////////////////////////////////////////////////////

$pdf->Cell(0,10,'Tabla "Inventario de Herramientas"',0,1,'C');

    $pdf->Cell(10, 10, 'id_h', 1, 0, 'C', 0);
    $pdf->Cell(50, 10, 'Nombre', 1, 0, 'C', 0);
    $pdf->Cell(70, 10, 'Descripcion', 1, 0, 'C', 0);
    $pdf->Cell(40, 10, 'Marca', 1, 0, 'C', 0);
    $pdf->Cell(20, 10, 'Fecha', 1, 1, 'C', 0);
    while($row = $result2->fetch_assoc()){
    $pdf->Cell(10, 10, $row['id_h'], 1, 0, 'C', 0);
    $pdf->Cell(50, 10, $row['nombre'], 1, 0, 'C', 0);
    $pdf->Cell(70, 10, $row['descripcion'], 1, 0, 'C', 0);
    $pdf->Cell(40, 10, $row['marca'], 1, 0, 'C', 0);
    $pdf->Cell(20, 10, $row['Fecha'], 1, 1, 'C', 0);
}
//////////////////////////////////////////////////////////////////

$pdf->Cell(0,10,'Tabla "Empleados"',0,1,'C');

    $pdf->Cell(40, 10, 'RFC', 1, 0, 'C', 0);
    $pdf->Cell(50, 10, 'Nombre', 1, 0, 'C', 0);
    $pdf->Cell(50, 10, 'A. Paterno', 1, 0, 'C', 0);
    $pdf->Cell(50, 10, 'A. Materno', 1, 1, 'C', 0);
    while($row = $result3->fetch_assoc()){
        $pdf->Cell(40, 10, $row['rfc'], 1, 0, 'C', 0);
        $pdf->Cell(50, 10, $row['nombre'], 1, 0, 'C', 0);
        $pdf->Cell(50, 10, $row['ape1'], 1, 0, 'C', 0);
        $pdf->Cell(50, 10, $row['ape2'], 1, 1, 'C', 0);    
    }
    $pdf->Cell(50, 10, 'Correo', 1, 0, 'C', 0);
    $pdf->Cell(20, 10, 'Telefono', 1, 0, 'C', 0);
    $pdf->Cell(50, 10, 'Calle', 1, 0, 'C', 0);
    $pdf->Cell(50, 10, 'Fracc/Col.', 1, 0, 'C', 0);
    $pdf->Cell(20, 10, 'Numero', 1, 1, 'C', 0);
    
    while($row = $result4->fetch_assoc()){
        $pdf->Cell(50, 10, $row['correo'], 1, 0, 'C', 0);
        $pdf->Cell(20, 10, $row['telefono'], 1, 0, 'C', 0);
        $pdf->Cell(50, 10, $row['calle'], 1, 0, 'C', 0);
        $pdf->Cell(50, 10, $row['fracc_col'], 1, 0, 'C', 0);
        $pdf->Cell(20, 10, $row['numero_casa'], 1, 1, 'C', 0);
}
$pdf->Cell(20, 10, 'Fecha', 1, 1, 'C', 0);
    while($row = $result5->fetch_assoc()){
        $pdf->Cell(20, 10, $row['Fecha'], 1, 1, 'C', 0);
    }

$pdf->Output('D','a.pdf');
?>