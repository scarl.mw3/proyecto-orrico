<?php  
 function fetch_data()  
 {  
      $output = '';  
      $conn = mysqli_connect("localhost", "mmdmadet_user1", "12345", "mmdmadet_madettabd");  
      $sql = "SELECT * FROM inventario_h ";  
      $result = mysqli_query($conn, $sql);  
      while($row = mysqli_fetch_array($result))  
      {       
      $output .= '<tr>  
                          <td align="center">'.$row["id_h"].'</td>  
                          <td align="center">'.$row["nombre"].'</td>  
                          <td align="center">'.$row["descripcion"].'</td>  
                          <td align="center">'.$row["marca"].'</td>  
                          <td align="center">'.$row["Fecha"].'</td>    

                     </tr>  
                          ';  
      }  
      return $output;  
 }  
 if(isset($_POST["generate_pdf"]))  
 {  
      require_once('library/tcpdf.php');  
      $obj_pdf = new TCPDF('P', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);  
      $obj_pdf->SetCreator(PDF_CREATOR);  
      $obj_pdf->SetTitle("Gen Rep");  
      $obj_pdf->SetHeaderData('', '', PDF_HEADER_TITLE, PDF_HEADER_STRING);  
      $obj_pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));  
      $obj_pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));  
      $obj_pdf->SetDefaultMonospacedFont('helvetica');  
      $obj_pdf->SetFooterMargin(PDF_MARGIN_FOOTER);  
      $obj_pdf->SetMargins(PDF_MARGIN_LEFT, '10', PDF_MARGIN_RIGHT);  
      $obj_pdf->setPrintHeader(false);  
      $obj_pdf->setPrintFooter(false);  
      $obj_pdf->SetAutoPageBreak(TRUE, 10);  
      $obj_pdf->SetFont('helvetica', '', 11);  
      $obj_pdf->AddPage();  
      $content = '';  
      $content .= '  
      <h1 align="center">Reporte de tabla "Herramientas"</h1><br /> 
      <table border="1" cellspacing="0" cellpadding="3" width="100%">  
           <tr>  
                <th align="center" ><b>Id</b></th>  
                <th align="center"><b>Nombre</b></th>  
                <th align="center"><b>Descripcion</b></th>  
                <th align="center"><b>Marca</b></th>  
                <th align="center"><b>Fecha</b></th>   

           </tr>  
      ';  
      $content .= fetch_data();  
      $content .= '</table>';  
      $obj_pdf->writeHTML($content);  
      $obj_pdf->Output('ReporteHerramientas.pdf', 'I');  
 }  
 ?>  
 <!DOCTYPE html>  
 <html>  
      <head>  
           <title>Generar Reporte</title>  
           <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />            
      </head>  
      <body>  
           <br />
           <div class="container">  
                <h1 > Generar Reporte</h1><br />  
                <div class="table-responsive">  
                    <div class="col-md-12" >
                     <form method="post">  
                          <input type="submit" name="generate_pdf" class="btn btn-success" value="Generar Reporte" />  
                     </form>  
                     </div>
                     <br/>
                     <br/>
                     <table class="table table-bordered">  
                          <tr>  
                               <th >Id</th>  
                               <th >Nombre</th>  
                               <th >Descripcion</th>  
                               <th >Marca</th>  
                               <th >Fecha</th>  

                          </tr>  
                     <?php  
                     echo fetch_data();  
                     ?>  
                      
                     </table>  
                </div>  
           </div>  
      </body>  
</html>